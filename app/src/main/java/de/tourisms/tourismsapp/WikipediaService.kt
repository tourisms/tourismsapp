package de.tourisms.tourismsapp

import android.location.Location
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL

class WikipediaService {

    private var baseUrl1 = "https://<<LANGUAGE>>.wikipedia.org/w/api.php?action=query&format=json&list=search&srsearch=intitle:<<TITLE>>&maxlag=1"
    private var baseUrl2 = "https://<<LANGUAGE>>.wikipedia.org/api/rest_v1/page/summary/<<QUERY>>?maxlag=1"
    private var baseUrl2_geo = "https://<<LANGUAGE>>.wikipedia.org/w/api.php?action=query&format=json&gscoord=<<LAT>>%7C<<LONG>>&gsradius=10000&gslimit=100&list=geosearch"
    private var headline : String? = null;
    private var description : String? = null
    private var imageUrl : String? = null;
    private var text : String? = null
    private var wikilink : String? = null



    fun getWikiTitle(language: String, title: String, location: Location,
                     callback: () -> Unit, errorCallback: () -> Unit) {
        doAsync {
            val url = URL(
                baseUrl1
                    .replace("<<LANGUAGE>>", language)
                    .replace("<<TITLE>>", title)
            )

            val textResponse = url.readText()
            val jsonMap = JSONObject(textResponse).toMap()

            // If there is a search Result



            if(!jsonMap.containsKey("query")
                ||!(jsonMap["query"] as Map<*, *>).containsKey("search")
                || ((jsonMap["query"] as Map<*, *>)["search"] as List<Map<*,*>>).size == 0
            ) {
                uiThread {
                    errorCallback();
                }
                return@doAsync
            }

            var search_results_list = (jsonMap["query"] as Map<*, *>)["search"] as List<Map<*,*>>;

            if (search_results_list.isNotEmpty()) {

                // more than one result: Filter by geocoordinates
                val geo_results = JSONObject(URL(baseUrl2_geo
                    .replace("<<LANGUAGE>>", language)
                    .replace("<<LAT>>", location.latitude.toString())
                    .replace("<<LONG>>", location.longitude.toString())).readText()).toMap()

                val geo_results_list : List<Map<String, *>> =  ((geo_results["query"] as Map<*,*>)["geosearch"] as List<Map<String,*>>)
                val pageids : List<String> = geo_results_list.map { x -> x["pageid"].toString() }

                search_results_list = search_results_list.filter { m -> pageids.contains(m["pageid"].toString()) }
            }

            if(search_results_list.isNotEmpty()) {

                val headline = ((jsonMap["query"] as Map<*, *>)["search"] as List<Map<*,*>>)[0]["title"] as String;
                val newQuery = headline.replace(' ', '_');


                val data = JSONObject(URL(baseUrl2.replace("<<LANGUAGE>>", language)
                    .replace("<<QUERY>>", newQuery)).readText()).toMap()

                // If there is an Image in the Search Result
                var thumbnailsource : String? = null;
                if(data.containsKey("thumbnail")
                    && (data["thumbnail"] as Map<*,*>).containsKey("source")) {

                    // Build HTML for Image
                    thumbnailsource = (data["thumbnail"] as Map<*,*>)["source"] as String
                }

                val description = data["description"] as String;
                val text = data["extract"] as String;
                val wikilink = "https://$language.wikipedia.org/wiki/$newQuery"


                uiThread {
                    // Set Headline
                    this@WikipediaService.headline = headline;
                    this@WikipediaService.description = description;
                    this@WikipediaService.imageUrl = thumbnailsource;
                    this@WikipediaService.text = text;
                    this@WikipediaService.wikilink = wikilink;

                    callback();
                }
            } else {
uiThread {
                errorCallback(); // no results
}
            }
        }
    }

    fun getHeadline() : String? {
        return headline;
    }

    fun getDescription() : String? {
        return description;
    }

    fun getImageUrl() : String? {
        return imageUrl;
    }

    fun getHtmlText() : String? {
        return text;
    }

    fun getWikiLink() : String? {
        return wikilink;
    }

    fun JSONObject.toMap(): Map<String, *> = keys().asSequence().associateWith {
        when (val value = this[it])
        {
            is JSONArray ->
            {
                val map = (0 until value.length()).associate { Pair(it.toString(), value[it]) }
                JSONObject(map).toMap().values.toList()
            }
            is JSONObject -> value.toMap()
            JSONObject.NULL -> null
            else            -> value
        }
    }
}