package de.tourisms.tourismsapp

import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.bottom_info_sheet.*
import java.io.IOException


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val REQUEST_CHECK_SETTINGS = 2
        public const val LANGUAGE = "de";
    }

    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private lateinit var placesService: PlacesService
    private lateinit var wikiService: WikipediaService
    private var locationUpdateState = false
    private var gotFirstLocation = false
    private lateinit var camFab: View
    private var initialCameraLocation = LatLng(51.9607, 7.6261)

    private lateinit var bottomSheetBehavior : BottomSheetBehavior<*>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        placesService = PlacesService(getString(R.string.google_maps_key))
        wikiService = WikipediaService()

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)

                lastLocation = p0.lastLocation
                if (!gotFirstLocation) {
                    placesService.setNearbyAttractionsMarker(map, lastLocation)
                    map.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(lastLocation.latitude, lastLocation.longitude), 15f
                        )
                    )
                    gotFirstLocation = true
                }


                val nearestPlace = placesService.getNearestAttraction(lastLocation)
                if (nearestPlace != null && lastLocation.distanceTo(nearestPlace.loc) < 150) {
                    //camFab.visibility = View.VISIBLE
                } else {
                    //camFab.visibility = View.INVISIBLE
                }
            }
        }

        val scanNearbyFab: View = findViewById(R.id.scan_nearby_fab)
        scanNearbyFab.setOnClickListener {
            placesService.setNearbyAttractionsMarker(map, lastLocation)
        }

        camFab = findViewById(R.id.info_fab)
        camFab.setOnClickListener {
            val intent = Intent(this, ArActivity::class.java)
            startActivity(intent)
        }

        bottomSheetBehavior =  BottomSheetBehavior.from(layout_bottom_sheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN;
        
        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {

            override fun onStateChanged(view: View, state: Int) {
                when (state) {
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        scanNearbyFab.visibility = View.INVISIBLE
                        camFab.visibility = View.INVISIBLE
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        scanNearbyFab.visibility = View.VISIBLE
                        camFab.visibility = View.VISIBLE
                    }
                }
            }

            override fun onSlide(view: View, p1: Float) {
            }
        })

        bottom_sheet_btn_openwiki.setOnClickListener {
            val uri: Uri =
                Uri.parse(wikiService.getWikiLink()) // missing 'http://' will cause crashed
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }

        createLocationRequest()
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        map.isMyLocationEnabled = true
    }

    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }
        task.addOnFailureListener { e ->
            if (e is ResolvableApiException) {
                try {
                    e.startResolutionForResult(this@MapsActivity, REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Catch error, send to Gulag
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                locationUpdateState = true
                startLocationUpdates()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    public override fun onResume() {
        super.onResume()
        if (!locationUpdateState) {
            startLocationUpdates()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.uiSettings.isZoomControlsEnabled = true
        map.setOnMarkerClickListener(this)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(initialCameraLocation, 12.5f))

        setUpMap()
    }

    private fun loadBottomSheet(language: String, title: String) {
        wikiService.getWikiTitle(language, title, lastLocation, {
            bottom_sheet_headline.text = wikiService.getHeadline();
            Picasso.get().load(wikiService.getImageUrl()).into(bottom_sheet_image);
            bottom_sheet_description.text = wikiService.getDescription()
            bottom_sheet_htmltext.text = wikiService.getHtmlText()


            //bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }, {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        });
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        if(p0?.title != null) {
            loadBottomSheet(LANGUAGE, p0.title)
        }
        return false;
    }

    private fun getAddress(latLng: LatLng): String {
        val geocoder = Geocoder(this)
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            if (null != addresses && addresses.isNotEmpty()) {
                address = addresses[0]
                for (i in 0 until address.maxAddressLineIndex) {
                    addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
                }
            }
        } catch (e: IOException) {
            Log.e("MapsActivity", e.localizedMessage)
        }

        return addressText
    }
}