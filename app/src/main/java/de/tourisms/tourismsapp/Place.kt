package de.tourisms.tourismsapp

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import org.json.JSONObject

class Place(
    val name: String,
    val loc: Location
) {}