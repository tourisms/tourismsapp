package de.tourisms.tourismsapp

import android.location.Location
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL

class PlacesService {

    private var baseUrl = "https://maps.googleapis.com/maps/api/"
    private var apiKey: String
    private var nearPlaces = ArrayList<Place>()

    constructor(apiKey: String) {
        this.apiKey = apiKey
    }

    fun setNearbyAttractionsMarker(map: GoogleMap, loc: Location, radius: Int = 15000) {
        doAsync {
            val url = URL(
                baseUrl
                        + "place/nearbysearch/json?key=" + apiKey
                        + "&location=" + loc.latitude + "," + loc.longitude
                        + "&radius=" + radius
                        + "&type=tourist_attraction"
                        + "&language=" + MapsActivity.LANGUAGE
            )

            val textResponse = url.readText()
            val jsonMap = JSONObject(textResponse).toMap()

            val rawPlaces = jsonMap["results"] as ArrayList<HashMap<String, *>>
            nearPlaces.clear()
            for (placeData in rawPlaces) {
                val geometryData = placeData["geometry"] as HashMap<String, *>
                val locationData = geometryData["location"] as HashMap<String, *>
                val locLat = locationData["lat"] as Double
                val locLong = locationData["lng"] as Double

                val placeName = placeData["name"] as String

                val loc = Location("loc")
                loc.latitude = locLat
                loc.longitude = locLong
                nearPlaces.add(Place(placeName, loc))
            }

            uiThread {
                map.clear()
                for (place in nearPlaces) {
                    placeAttractionMarker(map, place)
                }
            }
        }
    }

    fun getNearestAttraction(loc: Location): Place? {
        var nearestPlace: Place? = null
        var nearestDist: Float? = null
        for (place in nearPlaces) {
            if (nearestPlace == null || loc.distanceTo(place.loc) < nearestDist!!) {
                nearestPlace = place
                nearestDist = loc.distanceTo(place.loc)
            }
        }

        return nearestPlace
    }

    private fun placeAttractionMarker(map: GoogleMap, place: Place) {
        val markerOptions = MarkerOptions().position(LatLng(place.loc.latitude, place.loc.longitude))

        markerOptions.title(place.name)

        map.addMarker(markerOptions)
    }

    fun JSONObject.toMap(): Map<String, *> = keys().asSequence().associateWith {
        when (val value = this[it])
        {
            is JSONArray ->
            {
                val map = (0 until value.length()).associate { Pair(it.toString(), value[it]) }
                JSONObject(map).toMap().values.toList()
            }
            is JSONObject -> value.toMap()
            JSONObject.NULL -> null
            else            -> value
        }
    }
}